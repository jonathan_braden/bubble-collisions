import numpy as np

lattice_file="collision_line_exactic.dat"
nlat_lat=1024
file_1d="field_exactic_1d.dat"
nlat_1d=1024

a=np.genfromtxt(lattice_file,usecols=[1,2])
fld_l=np.reshape(a[:,1],(-1,nlat_lat))
t_l=np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(file_1d,usecols=[0,2])
fld_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

#fld_l=fld_l[:,nlat/2]
#fld_1d=fld_1d[:,nlat/2]
#t_l=t_l[:,nlat/2]
#t_1d=t_1d[:,nlat/2]

import matplotlib.pyplot as plt
plt.plot(t_1d[:,nlat_1d/2-1],fld_1d[:,nlat_1d/2-1],'b',linewidth=2,label='SO(2,1) Lattice')
plt.plot(t_l[:,nlat_lat/2-1],fld_l[:,nlat_lat/2-1],'r.',label='3D Lattice')
plt.legend()
plt.xlim(0,100.)
plt.xlabel(r'$mt$')
plt.ylabel(r'$\phi/\phi_0$')
