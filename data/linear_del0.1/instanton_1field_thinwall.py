import numpy as np
from scipy.optimize import fsolve

infile = "instanton_w0.35.dat"
nlat=512

delta=0.1
rinit=2.**0.5/delta

potp_l = lambda x : x*(x**2-1.) - delta
potp_c = lambda x : (x+delta)*(x**2-1.)**2

phi_guess=1.+0.5*delta
phit=fsolve(potp_l,phi_guess)
phi_guess=-1.+0.5*delta
phif=fsolve(potp_l,phi_guess)
print "Vacua are ",phit, phif
phit=phit[0.]
phif=phif[0.]

def thinwall_linear(x):
    f = 0.5*(phif-phit)*np.tanh((x-rinit)/2.**0.5) + 0.5*(phif+phit)
    return f
def thinwall_cubic(x):
    f = -np.tanh((x-1.5*rinit)/2.**0.5)
    return f

def thinwall_badvac(x):
    f = -np.tanh((x-rinit)/2.**0.5) + 0.5*delta
    return f

def eom(x):
    f = 0.
    return f

tau=np.genfromtxt(infile,usecols=[1])
phi=np.genfromtxt(infile,usecols=[2])

thinwall=thinwall_linear(tau)
badvac = thinwall_badvac(tau)

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

plt.xlabel(r'$r_E$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.plot(tau,phi,linewidth=1.5,label=r'Numerical')
plt.plot(tau,thinwall,linewidth=1.5,label=r'Thin-Wall')
plt.plot(tau,badvac,'r.',markersize=5,label=r'$\phi_{f/t}=\mp\phi_0+\delta/2$')
plt.xlim(0.,3./delta)
plt.legend(loc='upper right',fontsize=22,bbox_to_anchor=(0,0,1.1,1.1))
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('instanton_linear_del0.1.pdf')
plt.show()

coeffs=np.genfromtxt(infile,usecols=[4])
plt.plot(np.abs(coeffs),linewidth=2)
plt.xlabel(r'Mode Number $(i)$',fontsize=30)
plt.ylabel(r'$|c_i|$',fontsize=30)
plt.yscale('log')
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('instanton_linear_coeffs_del0.1.pdf')
plt.show()

# Finally show volation of equation of motion
a=np.genfromtxt('instanton_offgrid_linear_del0.1.dat',usecols=[0,2,3,4,5])
plt.plot(a[:,0],np.abs(a[:,4]),label='Numerical',linewidth=1.5)
a=np.genfromtxt('instanton_linear_del0.1_thinwall.dat',usecols=[0,2,3,4,5])
plt.plot(a[:,0],np.abs(a[:,4]),label='Thin-Wall',linewidth=1.5)
a=np.genfromtxt('instanton_linear_del0.1_badvac.dat',usecols=[0,2,3,4,5])
plt.plot(a[:,0],np.abs(a[:,4]),label=r'$\phi_{f/t}=\mp\phi_0+\delta/2$',linewidth=1.5)

plt.yscale('log')
plt.ylabel(r"$EOM$",fontsize=30)
plt.xlabel(r'$r_E$',fontsize=30)
plt.xlim(0,100)
plt.ylim(1.e-16,0.1)
plt.legend(loc='center right',fontsize=22)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('instanton_linear_del0.1_eom.pdf')
plt.show()
