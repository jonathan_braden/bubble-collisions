import numpy as np

infile="collision_line_exactic.dat"
nlat=1024

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[1])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-xvals[nlat/2-1] # center collision at x=0

# Make constant s surfaces in the (y,t) plane
svals=np.linspace(30.,80.,6)
chivals=np.linspace(-3.,3.)
x_s=[]
y_s=[]
for i in range(len(svals)):
    x_s.append(svals[i]*np.sinh(chivals))
    y_s.append(svals[i]*np.cosh(chivals))
x_chi=[]
y_chi=[]
svals=np.linspace(0.,90.,19)
chivals=[-2.,-0.8,-0.2,0.,0.2,0.8,2.]
for i in range(len(chivals)):
    x_chi.append(svals*np.sinh(chivals[i]))
    y_chi.append(svals*np.cosh(chivals[i]))

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')

cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\phi/\phi_0$',fontsize=30)#,rotation='horizontal')

for i in range(len(x_s)):
    plt.plot(x_s[i],y_s[i],'k',alpha=0.5)
#for i in range(len(x_chi)):
#    plt.plot(x_chi[i],y_chi[i],'k',alpha=0.5)

plt.xlim((-90.,90.))
plt.ylim((0.,90.))

plt.xlabel(r'$mx_{\perp}$', fontsize=30)
plt.ylabel(r'$mt$', fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('bubble_exact_transverse_del0.1.png')
plt.show()
