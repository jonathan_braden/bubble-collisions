import numpy as np

lattice_file_1="one_bubble_exact_wfluc.dat"
nlat_1=512
lattice_file_2="one_bubble_exact_badvac.dat"
nlat_2=512
file_1d="one_bubble_exact_1d.dat"
nlat_1d=1024

a=np.genfromtxt(lattice_file_1,usecols=[0,1,2])
fld_1=np.reshape(a[:,2],(-1,nlat_1))
x_1=np.reshape(a[:,0],(-1,nlat_1))
t_1=np.reshape(a[:,1],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[0,1,2])
x_2=np.reshape(a[:,0],(-1,nlat_2))
t_2=np.reshape(a[:,1],(-1,nlat_2))
fld_2=np.reshape(a[:,2],(-1,nlat_2))

a=np.genfromtxt(file_1d,usecols=[0,1,2])
fld_1d=np.reshape(a[:,2],(-1,nlat_1d))
x_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

times=[0,150,300]

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

for i in times:
#    print t_1d[i,0]
    p_1,=plt.plot(x_1[i,:]-x_1[i,nlat_1/2-1],fld_1[i,:],'b',linewidth=1.5,markersize=3)
    p_2,=plt.plot(x_2[i,:]-x_2[i,nlat_2/2-1],fld_2[i,:],'g',linewidth=1.5,markersize=3)
    p_3,=plt.plot(x_1d[i,:]-x_1d[i,nlat_1d/2-1],fld_1d[i,:],'r.',linewidth=1.5,markersize=3)
plt.legend([p_1,p_2,p_3],[r"3D Lattice, Exact Instanton",r"3D Lattice, Thin-Wall",r'SO(2,1) Lattice'],loc='center',bbox_to_anchor=(0.5,0.92),fontsize=20)
plt.xlim(-45.,45.)
plt.ylim(-1.,1.8)
plt.xlabel(r'$mx$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.subplots_adjust(left=0.15,bottom=0.15)
plt.savefig('one_bubble_linear_del0.1_tslices.pdf')
plt.show()
