import numpy as np

lattice_file="collision_line_exactic.dat"
nlat_lat=1024
file_1d="field_exactic_1d.dat"
nlat_1d=1024
lattice_badvac="collision_line_del0.1_badvac.dat"
lat_bv_noniso="collision_line_del0.1_badvac_badstencil.dat"
file_1d_badvac="field_badvac_1d.dat"

a=np.genfromtxt(lattice_file,usecols=[1,2])
fld_l=np.reshape(a[:,1],(-1,nlat_lat))
t_l=np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(file_1d,usecols=[0,2])
fld_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

a=np.genfromtxt(lattice_badvac,usecols=[1,2])
fld_l_bv = np.reshape(a[:,1],(-1,nlat_lat))
t_l_bv = np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(lat_bv_noniso,usecols=[1,2])
fld_l_bv_bs=np.reshape(a[:,1],(-1,nlat_lat))
t_l_bv_bs=np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(file_1d_badvac,usecols=[0,2])
fld_1d_bv=np.reshape(a[:,1],(-1,nlat_1d))
t_1d_bv=np.reshape(a[:,0],(-1,nlat_1d))

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

plt.plot(t_1d[:,nlat_1d/2-1],fld_1d[:,nlat_1d/2-1],'b',linewidth=2,label='SO(2,1), Exact IC')
plt.plot(t_l[:,nlat_lat/2-1],fld_l[:,nlat_lat/2-1],'g^',label='3D, Exact IC')
plt.plot(t_1d_bv[:,nlat_1d/2-1],fld_l[:,nlat_lat/2-1],'r.',label='SO(2,1), Approx. IC')
plt.plot(t_l_bv[:,nlat_lat/2-1],fld_l_bv[:,nlat_lat/2-1],'k',linewidth=1.5,label='3D, Approx. IC, Isotropic Stencil')
plt.plot(t_l_bv_bs[:,nlat_lat/2-1],fld_l_bv_bs[:,nlat_lat/2-1],color='orange',marker='v',markersize=3,linewidth=1.5,label='3D, Approx. IC, Nonisotropic Stencil')
plt.legend(loc='lower left')
plt.xlim(0,100.)
plt.xlabel(r'$mt$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.ylim(-3.5,3)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('field_origin_linear_del0.1_wbadvac.pdf')
plt.show()
