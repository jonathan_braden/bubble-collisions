import numpy as np

infile="one_bubble_exact_wfluc.dat"
nlat=512

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[1])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-xvals[nlat/2-1] # center collision at x=0

rinit=2.**0.5*10.
y_thinwall=np.linspace(0.,45.,101)
x_thinwall=np.sqrt(rinit**2+y_thinwall**2)

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')

cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\phi/\phi_0$',fontsize=30)#,rotation='horizontal')

plt.plot(x_thinwall,y_thinwall,'k--',linewidth=1.5)
plt.plot(-x_thinwall,y_thinwall,'k--',linewidth=1.5)

plt.xlim((-45.,45.))
plt.ylim((0.,45.))

plt.xlabel(r'$mx_{\perp}$', fontsize=30)
plt.ylabel(r'$mt$', fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig("one_bubble_linear_del0.1_plane.png")
plt.show()
