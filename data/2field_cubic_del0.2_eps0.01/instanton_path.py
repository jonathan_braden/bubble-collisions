import numpy as np

#infile = "/media/data/Code/pseudospec/bubbles/instanton_2field_eps0.1_del0.1_nocubic.dat"
infile = "instanton_2field_eps-0.01_del0.2_cubic_w0.5.dat"
nlat=100
delta=0.2
epsilon=-0.01
g2=1.

levels=np.linspace(-1.,1.5,26)
levs=[]
for i in range(len(levels)):
    levs.append(levels[i])
levs.append(3)
levs.append(4)
levs.append(5)

def potential_cubic(phi,chi,d,g,ep):
    f=0.25*(chi**2-1.)**2 + d*(chi**3/3.-chi) + 0.5*g*phi**2*(chi-1)**2 + ep*phi
    return f

def potential(phi,chi,d, g, ep):
    f=0.25*(chi**2-1.)**2 - d*(chi-1.) + 0.5*g*phi**2*(chi-1)**2 + ep*phi
    return f

tau=np.genfromtxt(infile,usecols=[1])
chi=np.genfromtxt(infile,usecols=[2])
phi=np.genfromtxt(infile,usecols=[3])

phivals=np.linspace(-0.8,0.8,101)
chivals=np.linspace(-1.6,2,201)

potvals=[]
for i in range(len(phivals)):
    potvals.append(potential_cubic(phivals[i],chivals,delta,g2,epsilon))

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

plt.contourf(chivals,phivals,potvals,levels, cmap=plt.cm.OrRd_r, extend="max")
cbar=plt.colorbar()
plt.xlabel(r'$\sigma/\sigma_0$', fontsize=30)
plt.ylabel(r'$\phi/\sigma_0$', fontsize=30)
plt.plot(chi,phi,linewidth=2,color='b')
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('2field_path_cubic_del0.2_eps-0.01.png')
plt.show()

plt.plot(tau,phi,linewidth=2,label=r'$\phi/\sigma_0$')
plt.plot(tau,chi,linewidth=2,label=r'$\sigma/\sigma_0$')
plt.xlabel(r'$r_E$',fontsize=30)
plt.ylabel(r'$\phi/\sigma_0,\sigma/\sigma_0$',fontsize=30)
plt.xlim(0,5./delta)
plt.ylim(-1.1,1.1)
leg=plt.legend(fontsize=24)
#plt.setp(leg.get_texts(),fontsize='30')
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('2field_fields_cubic_del0.2_eps-0.01.pdf')
plt.show()

chispec=np.genfromtxt(infile,usecols=[6])
phispec=np.genfromtxt(infile,usecols=[7])

plt.plot(np.abs(phispec),label=r'$\phi/\sigma_0$',linewidth=1.5)
plt.plot(np.abs(chispec),label=r'$\sigma/\sigma_0$',linewidth=1.5)
plt.xlabel(r'Mode Number ($i$)',fontsize=30)
plt.ylabel(r'$|c_i|$',fontsize=30)
plt.yscale('log')
leg=plt.legend(fontsize=24)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('2field_spec_cubic_del0.2_eps-0.01.pdf')
plt.show()
