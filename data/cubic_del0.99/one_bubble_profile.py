import numpy as np

lattice_file_1="collision_line_n512_lattice.dat"
nlat_1=512
lattice_file_2="collision_line_n1024.dat"
nlat_2=1024
file_1d="collision_line_1d.dat"
nlat_1d=1024

a=np.genfromtxt(lattice_file_1,usecols=[0,1,2])
fld_1=np.reshape(a[:,2],(-1,nlat_1))
x_1=np.reshape(a[:,0],(-1,nlat_1))
t_1=np.reshape(a[:,1],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[0,1,2])
x_2=np.reshape(a[:,0],(-1,nlat_2))
t_2=np.reshape(a[:,1],(-1,nlat_2))
fld_2=np.reshape(a[:,2],(-1,nlat_2))

a=np.genfromtxt(file_1d,usecols=[0,1,2])
fld_1d=np.reshape(a[:,2],(-1,nlat_1d))
x_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

times=[0,100,180]

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

for i in times:
    j=2*i  # correct sampling frequency
    print t_1d[i,0]
    p_1,=plt.plot(x_1[j,:]-x_1[j,nlat_1/2-1],fld_1[j,:],'b',linewidth=1.5,markersize=3)
    p_2,=plt.plot(x_2[i,:]-x_2[i,nlat_2/2-1],fld_2[i,:],'g',linewidth=1.5,markersize=3)
    p_3,=plt.plot(x_1d[i,:]-x_1d[i,nlat_1d/2-1],fld_1d[i,:],'r',linewidth=1.5)
plt.legend([p_1,p_2,p_3],[r"3D Lattice, $N_{lat}=512^3$",r"3D Lattice, $N_{lat}=1024^3$",r'SO(2,1) Lattice'],loc='upper center',bbox_to_anchor=(0,0,1.,1.15),fontsize=18)
plt.xlim(-45.,45.)
plt.ylim(-1.,2)
plt.xlabel(r'$mx$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.subplots_adjust(left=0.15,bottom=0.15)

plt.savefig('one_bubble_del0.99_latet.pdf')
