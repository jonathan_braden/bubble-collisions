import numpy as np

#infile="collision_line_n1024_lattice.dat"
#nlat=1024

infile="collision_line_n512_lattice.dat"
nlat=512

#infile="collision_line_1d.dat"
#nlat=

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[1])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-xvals[nlat/2-1] # center collision at x=0

# Make constant s surfaces in the (y,t) plane
svals=np.linspace(5.,40.,8)
chivals=np.linspace(-3.,3.)

x=[]
y=[]
for i in range(len(svals)):
    x.append(svals[i]*np.sinh(chivals))
    y.append(svals[i]*np.cosh(chivals))

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')

cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\phi/\phi_0$',fontsize=30)#,rotation='horizontal')

for i in range(len(svals)):
    plt.plot(x[i],y[i],'k',alpha=0.75)

plt.xlim((-45.,45.))
plt.ylim((0.,45.))

plt.xlabel(r'$mx_{\perp}$', fontsize=30)
plt.ylabel(r'$mt$', fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)

plt.savefig('one_bubble_del0.99_transverse.png')
plt.show()
