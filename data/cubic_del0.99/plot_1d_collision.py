import numpy as np

infile="two_bubble_1dsim.dat"
nlat=1024

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
tvals=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
xvals=np.reshape(a,(-1,nlat))

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')
#plt.pcolor(xvals,tvals,fld,cmap=cm.RdYlBu_r,vmin=-1.2,vmax=1.2)

cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\phi/\phi_0$',fontsize=30) #,rotation='horizontal')

plt.xlim((-65.,65.))
plt.ylim((0.,80.))

plt.xlabel(r'$mx_{\perp}$', fontsize=30)
plt.ylabel(r'$mt$', fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)

plt.savefig("two_bubble_1d_del0.99_collision.png")
plt.show()
