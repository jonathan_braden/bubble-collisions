import numpy as np
from scipy.optimize import fsolve

infile = "instanton.dat"
nlat=70

delta=0.99
rinit=1.5*2.**0.5/delta

potp_l = lambda x : x*(x**2-1.) - delta
potp_c = lambda x : (x+delta)*(x**2-1.)**2

phit=1.
phif=-1.

def thinwall_linear(x):
    f = 0.5*(phif-phit)*np.tanh((x-rinit)/2.**0.5) + 0.5*(phif+phit)
    return f
def thinwall_cubic(x):
    f = -np.tanh((x-1.5*rinit)/2.**0.5)
    return f

def thinwall_badvac(x):
    f = -np.tanh((x-rinit)/2.**0.5) + 0.5*delta
    return f

def eom(x):
    f = 0.
    return f

tau=np.genfromtxt(infile,usecols=[1])
phi=np.genfromtxt(infile,usecols=[2])

import matplotlib.pyplot as plt
plt.xlabel(r'$r_E$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.plot(tau,phi,linewidth=1.5)
plt.xlim(0.,50.)
plt.show()

coeffs=np.genfromtxt(infile,usecols=[4])
plt.plot(np.abs(coeffs),linewidth=2)
plt.xlabel(r'Mode Number $(i)$',fontsize=30)
plt.ylabel(r'$|c_i|$',fontsize=30)
plt.yscale('log')
plt.show()
