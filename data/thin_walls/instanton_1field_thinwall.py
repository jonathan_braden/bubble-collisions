import numpy as np
from scipy.optimize import fsolve

#infile = "instanton_w0.35.dat"
infiles = [ "instanton_linear_del0.005_w0.022_200modes.dat", "instanton_linear_del0.001_w0.0045_200modes.dat", "instanton_cubic_del0.01_w0.03_200modes.dat", "instanton_cubic_del0.005_w0.014_200modes.dat" ]
labels = [ r"$\delta = 0.005$, linear", r"$\delta=0.001$, linear", r"$\delta=0.01$ cubic", r"$\delta=0.005$, cubic" ]
nlat=512

delta=0.1
rinit=2.**0.5/delta

potp_l = lambda x : x*(x**2-1.) - delta
potp_c = lambda x : (x+delta)*(x**2-1.)**2

phi_guess=1.+0.5*delta
phit=fsolve(potp_l,phi_guess)
phi_guess=-1.+0.5*delta
phif=fsolve(potp_l,phi_guess)
print "Vacua are ",phit, phif
phit=phit[0.]
phif=phif[0.]

def thinwall_linear(x):
    f = 0.5*(phif-phit)*np.tanh((x-rinit)/2.**0.5) + 0.5*(phif+phit)
    return f
def thinwall_cubic(x):
    f = -np.tanh((x-1.5*rinit)/2.**0.5)
    return f

def eom(x):
    f = 0.
    return f

tau=[]
phi=[]
coeff=[]
for f in infiles:
    tau.append(np.genfromtxt(f,usecols=[1]))
    phi.append(np.genfromtxt(f,usecols=[2]))
    coeff.append(np.genfromtxt(f,usecols=[4]))

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

for i in range(len(tau)):
    plt.plot(tau[i],phi[i],linewidth=2,label=labels[i])
plt.xlabel(r'$r_E$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.legend(loc='center',fontsize=20)
plt.xlim(0,1800.)
plt.ylim(-1.2,1.2)
plt.subplots_adjust(left=0.15,bottom=0.15)
plt.savefig('thin_wall_bubbles.pdf')
plt.show()

for i in range(len(coeff)):
    plt.plot(np.abs(coeff[i]),linewidth=2,label=labels[i])
plt.xlim(0,150)
plt.yscale('log')
plt.ylabel(r'$|c_i|$',fontsize=30)
plt.xlabel(r'Mode Number $(i)$',fontsize=30)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.1,1.1),fontsize=24)
plt.subplots_adjust(left=0.15,bottom=0.15)
plt.savefig('thin_wall_coeffs.pdf')
plt.show()
