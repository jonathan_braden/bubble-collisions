import numpy as np

#infile = "/media/data/Code/pseudospec/bubbles/instanton_2field_eps0.1_del0.1_nocubic.dat"
infile = "instanton_2field_eps-0.01_del0.2_cubic.dat"
nlat=512
delta=0.2
epsilon=-0.01
g2=1.

levels=np.linspace(-1.,1.5,26)
levs=[]
for i in range(len(levels)):
    levs.append(levels[i])
levs.append(3)
levs.append(4)
levs.append(5)

def potential_cubic(phi,chi,d,g,ep):
    f=0.25*(chi**2-1.)**2 + d*(chi**3/3.-chi) + 0.5*g*phi**2*(chi-1)**2 + ep*phi

def potential(phi,chi,d, g, ep):
    f=0.25*(chi**2-1.)**2 - d*(chi-1.) + 0.5*g*phi**2*(chi-1)**2 + ep*phi
    return f

tau=np.genfromtxt(infile,usecols=[1])
chi=np.genfromtxt(infile,usecols=[2])
phi=np.genfromtxt(infile,usecols=[3])

phivals=np.linspace(-0.8,0.8,101)
chivals=np.linspace(-1.6,2,201)

potvals=[]
for i in range(len(phivals)):
    potvals.append(potential(phivals[i],chivals,delta,g2,epsilon))

import matplotlib.pyplot as plt
plt.contourf(chivals,phivals,potvals,levels, cmap=plt.cm.OrRd_r, extend="max")
cbar=plt.colorbar()
plt.xlabel(r'$\sigma$', fontsize=16)
plt.ylabel(r'$\phi$', fontsize=16)
plt.plot(chi,phi,linewidth=2,color='b')
plt.show()

plt.plot(tau,phi,linewidth=2,label=r'$\sigma$')
plt.plot(tau,chi,linewidth=2,label=r'$\phi$')
plt.xlabel(r'$\tau$',fontsize=16)
plt.ylabel(r'$\phi,\sigma$',fontsize=16)
plt.xlim(0,5./delta)
plt.ylim(-1.1,1.1)
plt.legend()
plt.show()

chispec=np.genfromtxt(infile,usecols=[6])
phispec=np.genfromtxt(infile,usecols=[7])

plt.plot(np.abs(chispec),label=r'$\sigma$')
plt.plot(np.abs(phispec),label=r'$\phi$')
plt.xlabel(r'Mode Number $i$',fontsize=16)
plt.ylabel(r'$|c_i|$',fontsize=16)
plt.yscale('log')
plt.legend()
