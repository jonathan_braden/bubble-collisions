import numpy as np
from scipy.optimize import fsolve

delvals=[0.99,0.98,0.97,0.95,0.92,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05]

infiles=[]
l=[]
for d in delvals:
    infiles.append("instanton_del"+str(d)+".dat")
    l.append(r"$\delta ="+str(d)+"$")

tau=[]
phi=[]
for f in infiles:
    a=np.genfromtxt(f,usecols=[1,2])
    tau.append(a[:,0])
    phi.append(a[:,1])

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

mycmap=plt.get_cmap('jet')
cNorm=colors.Normalize(vmin=0,vmax=len(delvals)-1)
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

plt.xlabel(r'$r_E$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
for i in range(len(tau)):
    curcol=scalarMap.to_rgba(i)
    plt.plot(tau[i],phi[i],color=curcol,linewidth=1.5,label=l[i])
plt.xlim(0.,70.)
plt.legend(loc='upper right',fontsize=18,bbox_to_anchor=(0,0,1.1,1.1))
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('instanton_1field_cubic_varydelta.pdf')
plt.show()

delout=np.linspace(0.,1.,51)
phi_true=np.ones(len(delout))
phi_out=[]
for i in range(len(delvals)):
    phi_out.append(phi[i][0])

plt.plot(delvals,phi_out,'ro',label=r'$\phi_{out}/\phi_0$')
plt.plot(delout,phi_true,label=r'$\phi_{true}/\phi_0$')
plt.xlabel(r'$\delta$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.xlim(0,1)
plt.ylim(-1.1,1.1)
plt.legend(loc='center left',fontsize=24)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('phitunnel_cubic_varydelta.pdf')
plt.show()
