import numpy as np
from scipy.optimize import fsolve

infile = "instanton.dat"
nlat=512

delta=0.1
rinit=2.**0.5/delta

potp_l = lambda x : x*(x**2-1.) - delta
potp_c = lambda x : (x+delta)*(x**2-1.)**2

phi_guess=1.+0.5*delta
phit=fsolve(potp_l,phi_guess)
phi_guess=-1.+0.5*delta
phif=fsolve(potp_l,phi_guess)
print "Vacua are ",phit, phif
phit=phit[0.]
phif=phif[0.]

def thinwall_linear(x):
    f = 0.5*(phif-phit)*np.tanh((x-rinit)/2.**0.5) + 0.5*(phif+phit)
    return f
def thinwall_cubic(x):
    f = -np.tanh((x-1.5*rinit)/2.**0.5)
    return f

def eom(x):
    f = 0.
    return f

tau=np.genfromtxt(infile,usecols=[1])
phi=np.genfromtxt(infile,usecols=[2])

thinwall=thinwall_linear(tau)

import matplotlib.pyplot as plt
plt.xlabel(r'$\tau$',fontsize=16)
plt.ylabel(r'$\phi$',fontsize=16)
plt.plot(tau,phi,linewidth=2,label=r'$Numerical$')
plt.plot(tau,thinwall,linewidth=2,label=r'$Thin-Wall$')
plt.xlim(0.,5./delta)
plt.legend(loc='upper right')
plt.show()

coeffs=np.genfromtxt(infile,usecols=[4])
plt.plot(np.abs(coeffs))
plt.xlabel(r'Mode Number $(i)$',fontsize=16)
plt.ylabel(r'$|c_i|$',fontsize=16)
plt.yscale('log')
plt.show()
