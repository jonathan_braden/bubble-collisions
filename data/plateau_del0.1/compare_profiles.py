import numpy as np

lattice_file_1="collision_line_del0.1_wfluc.dat"
nlat_1=1024
lattice_file_2="collision_line_del0.1_nofluc.dat"
nlat_2=1024
#file_1d="collision_line_1d.dat"
#nlat_1d=1024

a=np.genfromtxt(lattice_file_1,usecols=[0,1,2])
fld_1=np.reshape(a[:,2],(-1,nlat_1))
x_1=np.reshape(a[:,0],(-1,nlat_1))
t_1=np.reshape(a[:,1],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[0,1,2])
x_2=np.reshape(a[:,0],(-1,nlat_2))
t_2=np.reshape(a[:,1],(-1,nlat_2))
fld_2=np.reshape(a[:,2],(-1,nlat_2))

times=[150,200,250,350]  # Sync these to the times from Visit plots

import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

for i in times:
    print t_1[i,0]
    p_2,=plt.plot(x_2[i,::2]-x_2[i,nlat_2/2-1],fld_2[i,::2],'r^',markersize=4,markeredgecolor='r',markerfacecolor='r')
    p_1,=plt.plot(x_1[i,::2]-x_1[i,nlat_1/2-1],fld_1[i,::2],'k',linewidth=1.5,markersize=3)
 
plt.legend([p_1,p_2],[r"Fluctuations",r"No Fluctuations"],loc='center',bbox_to_anchor=(0.5,1.),fontsize=22)
plt.xlim(-90.,90.)
plt.ylim(-1.2,4.)
plt.xlabel(r'$mx_\perp$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('plateau_compare_transverse.pdf')
plt.show()

