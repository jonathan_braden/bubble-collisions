import numpy as np

lattice_file_1="collision_line_n512_lattice.dat"
nlat_1=512
lattice_file_2="collision_line_n1024.dat"
nlat_2=1024
file_1d="collision_line_1d.dat"
nlat_1d=1024

a=np.genfromtxt(lattice_file_1,usecols=[1,2])
fld_1=np.reshape(a[:,1],(-1,nlat_1))
t_1=np.reshape(a[:,0],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[1,2])
t_2=np.reshape(a[:,0],(-1,nlat_2))
fld_2=np.reshape(a[:,1],(-1,nlat_2))

a=np.genfromtxt(file_1d,usecols=[0,2])
fld_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

import matplotlib.pyplot as plt
plt.plot(t_1[::8,nlat_1/2-1],fld_1[::8,nlat_1/2-1],'g^',alpha=0.75,label='3D Lattice, N=512')
plt.plot(t_2[::4,nlat_2/2-1],fld_2[::4,nlat_2/2-1],'bv',alpha=0.75, label='3D Lattice, N=1024')
plt.plot(t_1d[::,nlat_1d/2-1],fld_1d[::,nlat_1d/2-1],'r',label='SO(2,1) Lattice',linewidth=1.5)
plt.legend(loc='lower right')
plt.xlim(0,90.)
plt.ylim(-1.,2.)
plt.xlabel(r'$mt$',fontsize=18)
plt.ylabel(r'$\phi/\phi_0$',fontsize=18)

plt.savefig('one_bubble_del0.99_center.pdf')
