import numpy as np
from scipy.optimize import fsolve

delvals=[0.3848,0.3845,0.384,0.38,0.37,0.35,0.33,0.25,0.2,0.15,0.1,0.05]

infiles=[]
l=[]
for d in delvals:
    infiles.append("instanton_linear_del"+str(d)+".dat")
    l.append(r"$\delta ="+str(d)+"$")

tau=[]
phi=[]
for f in infiles:
    a=np.genfromtxt(f,usecols=[1,2])
    tau.append(a[:,0])
    phi.append(a[:,1])

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

mycmap=plt.get_cmap('jet')
#cNorm=colors.Normalize(vmin=0.,vmax=delvals[0]) # normalize via delta
cNorm=colors.Normalize(vmin=0,vmax=len(delvals))#normalize via counting
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

plt.xlabel(r'$r_E$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
for i in range(len(tau)):
    curcol=scalarMap.to_rgba(i)
    plt.plot(tau[i],phi[i],color=curcol,linewidth=1.5,label=l[i])
plt.xlim(0.,50.)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.1,1.1),fontsize=20)
plt.subplots_adjust(bottom=0.15,left=0.15)
plt.savefig('instanton_1field_linear_varydelta.pdf')
plt.show()

# Now extract the value at the origin (here approximated as innermost collocation point

phi_in=[]
for i in range(len(delvals)):
    phi_in.append(phi[i][0])

phi_true=[]
deltrue=np.linspace(0.,2./3.**1.5,51)
for d in deltrue:
    potp = lambda x : x*(x**2-1.) - d
    phi_true.append(fsolve(potp,[1])[0])

plt.plot(delvals,phi_in,'ro',markersize=5, label=r'$\phi_{out}/\phi_0$')
plt.plot(deltrue,phi_true,'b',linewidth=1.5, label=r'$\phi_{true}/\phi_0$')
plt.xlabel(r'$\delta$',fontsize=30)
plt.ylabel(r'$\phi/\phi_0$',fontsize=30)
plt.legend(loc='center left',fontsize=24)
plt.xlim(0.,0.4)
plt.subplots_adjust(left=0.15,bottom=0.15)
plt.savefig('phitunnel_linear_varydelta.pdf')
plt.show()
