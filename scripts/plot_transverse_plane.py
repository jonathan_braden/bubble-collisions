import numpy as np

infile="collision_line.dat"
nlat=1024

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[1])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-xvals[nlat/2-1] # center collision at x=0

# Make constant s surfaces in the (y,t) plane
svals=np.linspace(30.,80.,6)
chivals=np.linspace(-3.,3.)

x=[]
y=[]
for i in range(len(svals)):
    x.append(svals[i]*np.sinh(chivals))
    y.append(svals[i]*np.cosh(chivals))

import matplotlib.pyplot as plt
import matplotlib.cm as cm

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')

cb=plt.colorbar(orientation='vertical')
cb.ax.set_ylabel(r'$\frac{\phi}{\phi_0}$',fontsize=24,rotation='horizontal')

for i in range(len(svals)):
    plt.plot(x[i],y[i],'k',alpha=0.5)

plt.xlim((-90.,90.))
plt.ylim((0.,90.))

plt.xlabel(r'$mx_{\perp}$', fontsize=20)
plt.ylabel(r'$mt$', fontsize=20)
