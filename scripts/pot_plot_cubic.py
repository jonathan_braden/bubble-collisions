#!/usr/bin/python
# Filename: pot_plot.py

#
# Make the plot of the potential that is being used in the paper
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

# Set some plot properties
# font size
fsize = 20

#
# Define the potential parameters in here
#
delvals=(0., 0.1, 0.2, 0.5, 0.9, 0.99)
print len(delvals)
dellabels=(r'$0$',r'$\frac{1}{10}$',r'$\frac{1}{5}$',r'$\frac{1}{2}$',r'$\frac{9}{10}$',r'$0.99$')

# Define the potential
def pot(x, d):
    return 0.25*(x**2-1)**2 + d*(x**3/3. - x + 2./3.)

#
# Now make the plot
#
plt.ylabel(r'$V(\phi)/\lambda \phi_0^4$', fontsize=30)
plt.xlabel(r'$\phi/\phi_0$', fontsize=30)

xvals = np.linspace(-1.5,1.5,101)
for i in range(len(delvals)):
    yvals=pot(xvals, delvals[i])
    plt.plot(xvals, yvals, linewidth=2.0, label=r'$\delta=$'+dellabels[i])

plt.subplots_adjust(bottom=0.15)
plt.subplots_adjust(left=0.16)
#plt.legend(frameon=False)
plt.legend(bbox_to_anchor=(0,0,1.1,1.1),fontsize=22)

plt.savefig('potential_cubic.pdf', bbox_inches=0)
