PaperPlots=True
MoviePlots=False

# Fill in color tables up here
def MakeRGBColorTable(name,ct):
    ccpl = ColorControlPointList()
    for pt in ct:
        p = ColorControlPoint()
        p.colors = (pt[0]*255, pt[1]*255, pt[2]*255, 255)
        p.position = pt[3]
        ccpl.AddControlPoints(p)
    AddColorTable(name,ccpl)


# Adjust as needed
#length=260./2.**0.5
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_exact_ic_1024_wline/output/"
#base_file="bubble_contour_twowell_exactic_"
#levels=(0.25,0.5,1.)
#tslices=(0,400,600,800)

#length=260./2.**0.5
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_thinwall_badvac/output_symp6/"
#base_file="bubble_contour_twowell_thinwall_"
#levels=(0.25,0.5,1.)
#tslices=(0,200,300,400)

length=260./2.**0.5
rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_exact_ic_1024_wfluc/output/"
base_file="bubble_contour_twowell_wfluc_"
levels=(0.25,0.5,1.)
tslices=(250,275,300,325,350,375,400) 

figheight=1024
figwidth=1024

varname="rhonorm"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0

a.backgroundMode=1
a.gradientBackgroundStyle = 0
a.gradientColor1 = (192,192,192,255)
a.gradientColor2 = (255,255,255,255)

SetAnnotationAttributes(a)

AddPlot("Contour",varname)
ca=ContourAttributes()
ca.contourNLevels=len(levels)
ca.contourMethod=ca.Value
ca.contourValue=levels
ca.SetMultiColor(0, (255,0,0,25))
ca.SetMultiColor(1, (0,255,0,50))
ca.SetMultiColor(2, (0,0,255,100))
ca.SetMultiColor(3, (255,0,255,125))
ca.SetMultiColor(4, (0,128,128,150))
SetPlotOptions(ca)

AddOperator("Box")
bo=GetOperatorOptions(0)  # check required index)
bo.minx=0
bo.maxx=length
bo.miny=0
bo.maxy=length
bo.minz=0.5*length-0.25*length
bo.maxz=0.5*length+0.25*length
SetOperatorOptions(bo)

#SetViewExtentsType("actual")

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.04,0.98)
l.xScale=0.5
l.yScale=0.7
l.fontBold=1
l.fontHeight=0.15
l.fontFamily=2

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.15,0.05)
ts.height=0.1
ts.width=0.5


sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=1
SetSaveWindowAttributes(sw)

DrawPlots()
v=GetView3D()
v.viewUp=(0,1,0)
v.viewNormal = (0.2,0.1,0.5)
v.focus=(length/2.-3.*length/16.,length/2.-3.*length/16.,length/2.)
SetView3D(v)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
#        DrawPlots()
        SetPlotOptions(ca) # Evil hack to get the axes to be visible
        SaveWindow()

if MoviePlots:
    for i in range(TimeSliderGetNStates()):
        SetTimeSliderState(i)
        DrawPlots()
        SetPlotOptions(ca)
        SaveWindow()
