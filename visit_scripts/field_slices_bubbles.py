PaperPlots=True

def MakeRGBColorTable(name,ct):
    ccpl = ColorControlPointList()
    for pt in ct:
        p = ColorControlPoint()
        p.colors = (pt[0]*255, pt[1]*255, pt[2]*255, 255)
        p.position = pt[3]
        ccpl.AddControlPoints(p)
    AddColorTable(name,ccpl)

cpoints = [ ( 0., 0., 1., 0.),
            ( 1, 1, 1, 0.25),
            ( 1. ,0., 0., 0.5),
            ( 0., 1., 0., 1.) ]
MakeRGBColorTable("plateau", cpoints)

cpoints = [ ( 0., 0., 1., 0.),
            ( 1, 1, 1, 0.125),
            ( 1, 0, 0, 0.25),
            ( 0. ,1., 0., 0.5),
            ( 1., 1., 0., 1.) ]
MakeRGBColorTable("plateau_tilt", cpoints)
   
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_thinwall_badvac/output_symp6/"
#length=260./2.**0.5
#width=length
#xcent=width/2.
#mycolortable="difference"
#minval,maxval=-1.2,1.2
#base_file="bubble_field_twowell_thinwall_"
#tslices=(0,200,300,400)

#length=260./2.**0.5
#width=length
#xcent=width/2.
#minval,maxval=-1.2,1.2
#mycolortable="difference"
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_exact_ic_1024_wfluc/output/"
#base_file="bubble_field_twowell_wfluc_"
#tslices=(250,275,300,325,350,375,400) 

#length=260./2.**0.5
#width=length
#xcent=width/2.
#mycolortable="difference"
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_exact_ic_1024_wline/output/"
#base_file="bubble_field_twowell_exactic_"
#minval,maxval = -1.2,1.2
#tslices=(0,400,600,800)

#length=260./2.**0.5
#width=length
#xcent=width/2.
#mycolortable="difference"
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/thick_wall_collision_wline_fluc/output/"
#base_file="bubble_field_twowell_thickwall_"
#minval,maxval = -1.2,1.2
#tslices=(0,340,380,700)

# In this one I could optionally use a different run...
#length=13.*(3./2.)*2.**0.5*5.
#width=length
#xcent=width/2.
#mycolortable="difference"
#rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_2field_exact/output/"
#base_file="bubble_2field_tunnel_"
#minval,maxval = -1.2,1.2
#tslices=(200,240,300,400)

length=260./2.**0.5
width=length
xcent=width/2.
mycolortable="plateau"
rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_plateau_wline/output/"
base_file="bubble_plateau_"
minval,maxval = -1.,3.
tslices=(0,150,250,380,400)

length=260./2.**0.5
width=length
xcent=width/2.
mycolortable="plateau_tilt"
rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_plateau_tilt/output/"
base_file="bubble_plateau_tilt_"
minval,maxval = -1.,7.
tslices=(0,150,200,250,300,380,400)

figheight=1024
figwidth=1024

varname="phi1"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
a.axes3D.bboxLocation=(0.,length,0.,length,length/2.,length)
a.axes3D.setBBoxLocation=1

a.backgroundMode=1
a.gradientBackgroundStyle = 0
a.gradientColor1 = (192,192,192,255)
a.gradientColor2 = (255,255,255,255)

SetAnnotationAttributes(a)

AddPlot("Pseudocolor",varname)
pa=PseudocolorAttributes()
pa.colorTableName=mycolortable
pa.minFlag=1
pa.maxFlag=1
pa.min=minval
pa.max=maxval
pa.smoothingLevel=2
SetPlotOptions(pa)

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.05,0.95)
l.xScale=0.5
l.yScale=3
l.fontBold=1
l.fontHeight=0.12
l.fontFamily=2

AddOperator("Slice")
sa=SliceAttributes()
sa.originType=sa.Intercept
sa.originIntercept=length/2.
sa.axisType=sa.YAxis
sa.project2d=1
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta=TransformAttributes()
ta.doScale=1
ta.scaleZ=5
ta.doTranslate=1
ta.translateZ=length
SetOperatorOptions(ta)

AddPlot("Pseudocolor",varname)
pa.legendFlag=0
SetPlotOptions(pa)

AddOperator("Box")
bo=GetOperatorOptions(0)  # check required index)
bo.minx=0
bo.maxx=length
bo.miny=0
bo.maxy=length*0.5
bo.minz=0.
bo.maxz=length
SetOperatorOptions(bo)

AddOperator("Slice")
sa.originIntercept=length/2.
sa.axisType=sa.ZAxis
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta.doRotate=0
ta.doScale=1
ta.doTranslate=0
ta.scaleZ=7
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=1
ta.rotateAxis=(1,0,0)
ta.rotateAmount=-90
ta.doScale=0
ta.doTranslate=0
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=0
ta.doTranslate=1
ta.translateY=9.*length/16.
SetOperatorOptions(ta)

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.2,0.05)
ts.height=0.1
ts.width=0.5

DrawPlots()

v=GetView3D()
v.viewNormal = (-.3,-0.3,-0.7)
v.viewUp=(0,-1,0)
v.focus=(length/2.-3.*length/16.,length/2.,length/2.-length/8.)
v.imageZoom=1.1
SetView3D(v)

#v=GetView3D()
#v.focus=(length/2.,width/2.,3.*length/8.)
#SetView3D(v)

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=0
SetSaveWindowAttributes(sw)

#for tcur in range(500):
#    SetTimeSliderState(tcur)
#    SaveWindow()

SetTimeSliderState(200)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        SaveWindow()
