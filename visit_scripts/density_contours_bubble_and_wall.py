PaperPlots=True
MoviePlots=False

rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/bubble_and_wall/bulk_l1e-4_newic/output/"
tslices=[250,450,600]
levels=(0.5,2.5,5.)
base_file="density_contours_bubble_wall_"
length=13.*2.**0.5*10.

figheight=720
figwidth=720

varname="rhonorm"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0

a.backgroundMode=1
a.gradientBackgroundStyle=0
a.gradientColor1 = (192,192,192,255)
a.gradientColor2 = (255,255,255,255)

SetAnnotationAttributes(a)

AddPlot("Contour",varname)
ca=ContourAttributes()
ca.contourNLevels=len(levels)
ca.contourMethod=ca.Value
ca.contourValue=levels
ca.SetMultiColor(0, (255,0,0,25))
ca.SetMultiColor(1, (0,255,0,50))
ca.SetMultiColor(2, (0,0,255,100))
ca.SetMultiColor(3, (255,0,255,125))
ca.SetMultiColor(4, (0,128,128,150))
SetPlotOptions(ca)

# Select subset of data
AddOperator("Box")
bo=GetOperatorOptions(0)
bo.minx=0.5*length
bo.maxx=length
bo.miny=0
bo.maxy=length
bo.minz=10.
bo.maxz=length-10.
SetOperatorOptions(bo)
DrawPlots()

v=GetView3D()
v.viewNormal = (0.5,0.5,0.3)
v.viewUp=(0,0,1)
v.focus=(90,90,0.25*length)
SetView3D(v)

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.04,0.98)
l.xScale=0.5
l.yScale=0.7
l.fontBold=1
l.fontHeight=0.15
l.fontFamily=2

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.15,0.05)
ts.height=0.1
ts.width=0.5


sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=1
SetSaveWindowAttributes(sw)

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        DrawPlots()
        SetPlotOptions(ca) # Evil hack to get the axes to be visible
        SaveWindow()

if MoviePlots:
    for i in range(TimeSliderGetNStates()):
        SetTimeSliderState(i)
        DrawPlots()
        SetPlotOptions(ca)
        SaveWindow()
