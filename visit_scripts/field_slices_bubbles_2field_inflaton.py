PaperPlots=True

def MakeRGBColorTable(name,ct):
    ccpl = ColorControlPointList()
    for pt in ct:
        p = ColorControlPoint()
        p.colors = (pt[0]*255, pt[1]*255, pt[2]*255, 255)
        p.position = pt[3]
        ccpl.AddControlPoints(p)
    AddColorTable(name,ccpl)

cpoints = [ ( 1., 1., 0., 1./12.),
            ( 1. ,0., 0., 1./6.),
            ( 1. ,1., 1., 0.5),
            ( 0., 0., 1., 5./6.),
            ( 0., 1., 1., 11./12.) ]
MakeRGBColorTable("plateau", cpoints)

length=13.*(3./2.)*2.**0.5*5.
width=length
xcent=width/2.
mycolortable="hot"
rootdir="/mnt/scratch-lustre/jbraden/bubble_collisions/collision_2field_exact/output/"
base_file="bubble_2field_inflaton_"
minval,maxval = -5.,0.
tslices=(200,240,300,400)

figheight=1024
figwidth=1024

varname="phi2"
dbname=varname+"-*.bov database"
db=rootdir+dbname

OpenDatabase(db)

a=GetAnnotationAttributes()
a.databaseInfoFlag=0
a.userInfoFlag=0
a.axes3D.xAxis.title.visible=0
a.axes3D.yAxis.title.visible=0
a.axes3D.zAxis.title.visible=0
a.axes3D.triadFlag=0
a.axes3D.axesType=a.FurthestTriad
a.axes3D.bboxFlag=0
a.axes3D.bboxLocation=(0.,length,0.,length,0.,length)
a.axes3D.setBBoxLocation=1

a.backgroundMode=1
a.gradientBackgroundStyle = 0
a.gradientColor1 = (192,192,192,255)
a.gradientColor2 = (255,255,255,255)

SetAnnotationAttributes(a)

AddPlot("Pseudocolor",varname)
pa=PseudocolorAttributes()
pa.colorTableName=mycolortable
pa.minFlag=1
pa.maxFlag=1
pa.min=minval
pa.max=maxval
pa.smoothingLevel=2
SetPlotOptions(pa)

objs=GetAnnotationObjectNames()
l=GetAnnotationObject(objs[0])
l.drawTitle=0
l.drawMinMax=0
l.orientation=l.VerticalRight
l.numberFormat="%# -5.2g"
l.managePosition=1
l.position=(0.05,0.95)
l.xScale=0.5
l.yScale=3
l.fontBold=1
l.fontHeight=0.12
l.fontFamily=2

AddOperator("Slice")
sa=SliceAttributes()
sa.originType=sa.Intercept
sa.originIntercept=length/2.
sa.axisType=sa.YAxis
sa.project2d=1
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta=TransformAttributes()
ta.doScale=1
ta.scaleZ=5
ta.doTranslate=0
ta.translateZ=length
SetOperatorOptions(ta)

AddPlot("Pseudocolor",varname)
pa.legendFlag=0
SetPlotOptions(pa)

AddOperator("Slice")
sa.originIntercept=length/2.
sa.axisType=sa.ZAxis
SetOperatorOptions(sa)

AddOperator("Elevate")

AddOperator("Transform")
ta.doRotate=0
ta.doScale=1
ta.doTranslate=0
ta.scaleZ=7
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=1
ta.rotateAxis=(1,0,0)
ta.rotateAmount=-90
ta.doScale=0
ta.doTranslate=0
SetOperatorOptions(ta)

AddOperator("Transform")
ta.doRotate=0
ta.doTranslate=1
ta.translateY=9.*length/16.
SetOperatorOptions(ta)

ts=CreateAnnotationObject("TimeSlider")
ts.position=(0.2,0.05)
ts.height=0.1
ts.width=0.5

DrawPlots()

SetTimeSliderState(400)

v=GetView3D()
v.viewNormal = (0.6,0.7,0.5)
v.viewUp=(0,0,1)
v.focus=(length/2.+3.*length/16.,length/2.,length/2.-3.*length/16.)
v.imageZoom=0.9
SetView3D(v)

sw=GetSaveWindowAttributes()
sw.format=sw.PNG
sw.width=figwidth
sw.height=figheight
sw.fileName=base_file
sw.resConstraint=sw.NoConstraint
sw.screenCapture=0
SetSaveWindowAttributes(sw)

#for tcur in range(500):
#    SetTimeSliderState(tcur)
#    SaveWindow()

if PaperPlots:
    for tcur in tslices:
        SetTimeSliderState(tcur)
        SetView3D(v)
        SaveWindow()
