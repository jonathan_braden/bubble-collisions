#!/usr/bin/env python
import sys
print sys.argv
showPreview=(sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from matplotlib.ticker import MaxNLocator

from transverse_plane_plot import *

fig_ext='.pdf'
svals=np.linspace(25.,80.,10)

def main():
    basedir='../data/cubic_del0.99/'
    infile=basedir+"two_bubble_1dsim.dat"
    nlat = 1024
    xvals,tvals,fld = gen_transverse_data(infile,nlat,[1,0,2])
    eps=0.1
    cvals = np.arange(-1.2,1.2+eps,eps)
    transverse_plane_plot(xvals,tvals,fld,cvals,x_label=r'$mx_\parallel$',xlims=(-65.,65.),ylims=(0.,80.))

    plt.savefig("two_bubble_1d_del0.99_collision"+fig_ext)
    if showPreview:
        plt.show()
    plt.clf()

    infile=basedir+"two_bubble_fluc_old.dat"
    nlat=1024
    xvals,tvals,fld = gen_transverse_data(infile,nlat,[0,1,2])
    xvals = center_positions(xvals)
    eps=0.1
    cvals=np.arange(-1.2,1.2+eps,eps)
    transverse_plane_plot(xvals,tvals,fld,cvals,xlims=(-90.,90.),ylims=(0.,90.))
    add_fixed_s(svals)

    plt.savefig("twobubble_thickwall_transverse"+fig_ext)
    if showPreview:
        plt.show()
    plt.clf()

    infile=basedir+"collision_line_n512_lattice.dat"
    nlat=512
    xvals,tvals,fld = gen_transverse_data(infile,nlat,[0,1,2])
    xvals=center_positions(xvals)
    transverse_plane_plot(xvals,tvals,fld,cvals,xlims=(-45.,45.),ylims=(0.,45.))
    add_fixed_s(np.linspace(5.,40.,8))

    plt.savefig('one_bubble_del0.99_transverse'+fig_ext)
    if showPreview:
        plt.show()
    plt.clf()

    basedir='../data/linear_del0.1/'
    infile=basedir+'collision_line_del0.1_badvac.dat'
    nlat=1024
    xvals,tvals,fld = gen_transverse_data(infile,nlat,[0,1,2])
    xvals = center_positions(xvals)
    transverse_plane_plot(xvals,tvals,fld,cvals,xlims=(-90.,90.),ylims=(0.,90.))
    add_fixed_s(svals)
    plt.savefig('bubble_badvac_transverse_del0.1'+fig_ext)
    if showPreview:
        plt.show()
    plt.clf()

    infile=basedir+'collision_line_exactic.dat'
    nlat=1024
    xvals,tvals,fld = gen_transverse_data(infile,nlat,[0,1,2])
    xvals = center_positions(xvals)
    transverse_plane_plot(xvals,tvals,fld,cvals,xlims=(-90.,90.),ylims=(0.,90.))
    add_fixed_s(svals)
    plt.savefig('bubble_exact_transverse_del0.1'+fig_ext)
    if showPreview:
        plt.show()
    plt.clf()

def add_fixed_s(svals):
    chivals=np.linspace(-3.,3.)
    x=[]
    y=[]
    for i in range(len(svals)):
        x.append(svals[i]*np.sinh(chivals))
        y.append(svals[i]*np.cosh(chivals))
    for i in range(len(svals)):
        plt.plot(x[i],y[i],'k',alpha=1.,linewidth=0.5)
    return

def center_positions(xvals):
    for i in range(len(xvals)):
        xvals[i,:] = xvals[i,:] - xvals[i,len(xvals[i])/2-1]
    return xvals

if __name__=='__main__':
    main()
