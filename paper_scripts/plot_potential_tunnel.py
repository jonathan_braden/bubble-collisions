#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

basedir='../data/2field_cubic_del0.2_eps0.01/'
infile=basedir+"instanton_offgrid.dat"

delta=0.2
g2=1.
eps=-0.01

#finish this
def potential_cubic(x,y):
    f = 0.25*(y**2-1.)**2 + delta*(y**3/3.-y+2./3.) + 0.5*g2*(y-1.)**2*x**2 + eps*x
    return f

def potential_onefield(y):
    f = 0.25*(y**2-1.)**2 + delta*(y**3/2.-y+2./3.)

a=np.genfromtxt(infile,usecols=[0,1,2])
tvals=a[:,0]
phi=a[:,2]
chi=a[:,1]
nlat=len(phi)

# To do: output the derivatives directly in my spectral code
# This is done for single-field, add to two field
dphi=np.diff(phi)
dchi=np.diff(chi)
dsigma=(dphi**2+dchi**2)**0.5
dsigma=dsigma[::-1]
sigmacur=0.
sigma=[]
for i in range(len(dphi)):
    sigmacur = sigmacur + dsigma[i]
    sigma.append(sigmacur)
phiave=0.5*(phi[:nlat-1]+phi[1:nlat])
chiave=0.5*(chi[:nlat-1]+chi[1:nlat])
veff=potential_cubic(phiave,chiave)

v_fixed=potential_cubic(phiave[len(phiave)-1],chiave[::-1])
v_fixed_sigma=potential_cubic(phiave[len(phiave)-1],np.array(sigma)-1.)
v_phi0=potential_cubic(0.,chiave[::-1])
#plt.plot(chiave[::-1]+1.,v_fixed,'r.',label=r'$V(\phi_{fv},\chi)$')
#plt.plot(sigma,v_fixed,'b.',label=r'$V(\phi_{fv},)$')
#plt.plot(sigma,v_fixed_sigma,'g',label=r'$V(\phi_{fv},)$')
#plt.plot(chiave[::-1]+1.,v_fixed_sigma,'g.',label='$V(\phi_{fv},\sigma)$')
plt.plot(sigma[::10],v_phi0[::10],'ro',label=r'$V(0,\chi_{eff})$')
plt.plot(sigma,veff[::-1],'b',label=r'$V(\phi,\sigma)$')
plt.xlabel(r'$\chi_{eff}/\sigma_0$')
plt.ylabel(r'$V/\lambda_\sigma\sigma_0^4$')
plt.legend(bbox_to_anchor=(0,0,1.1,1.1))

plt.xlim(0,2.1)
plt.ylim(-0.05,0.5)
plt.gca().yaxis.set_ticks([0,0.2,0.4])

#plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(), plt.gca())
plt.gca().set_position([0.15,0.17,0.9-0.15,0.9-0.15])
plt.savefig('sigma_eff_potential.pdf')
if showPreview:
    plt.show()
plt.clf()
