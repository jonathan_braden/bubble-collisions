#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

basedir='../data/linear_del0.1/'
infile=basedir+"collision_line_exactic.dat"
nlat=1024

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[1])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-xvals[nlat/2-1] # center collision at x=0

# Make constant s surfaces in the (y,t) plane
svals=np.linspace(30.,80.,6)
chivals=np.linspace(-3.,3.)
x_s=[]
y_s=[]
for i in range(len(svals)):
    x_s.append(svals[i]*np.sinh(chivals))
    y_s.append(svals[i]*np.cosh(chivals))
x_chi=[]
y_chi=[]
svals=np.linspace(0.,90.,19)
chivals=[-2.,-0.8,-0.2,0.,0.2,0.8,2.]
for i in range(len(chivals)):
    x_chi.append(svals*np.sinh(chivals[i]))
    y_chi.append(svals*np.cosh(chivals[i]))

import matplotlib.cm as cm

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
c=plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')
myplt.insert_rasterized_contour_plot(c)

cb=plt.colorbar(orientation='vertical',ticks=[-1,0,1],extend='both')
cb.ax.set_ylabel(r'$\phi/\phi_0$')#,rotation='horizontal')
cb.solids.set_rasterized(True)

for i in range(len(x_s)):
    plt.plot(x_s[i],y_s[i],'k',alpha=0.5)
#for i in range(len(x_chi)):
#    plt.plot(x_chi[i],y_chi[i],'k',alpha=0.5)

plt.xlim((-90.,90.))
plt.ylim((0.,90.))

plt.xlabel(r'$mx_{\perp}$')
plt.ylabel(r'$mt$')

plt.tight_layout(); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('bubble_exact_transverse_del0.1.pdf')
if showPreview:
    plt.show()
plt.clf()
