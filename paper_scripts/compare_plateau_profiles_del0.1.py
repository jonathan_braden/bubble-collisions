#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

basedir='../data/plateau_del0.1/'
lattice_file_1=basedir+"collision_line_del0.1_wfluc.dat"
nlat_1=1024
lattice_file_2=basedir+"collision_line_del0.1_nofluc.dat"
nlat_2=1024

a=np.genfromtxt(lattice_file_1,usecols=[0,1,2])
fld_1=np.reshape(a[:,2],(-1,nlat_1))
x_1=np.reshape(a[:,0],(-1,nlat_1))
t_1=np.reshape(a[:,1],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[0,1,2])
x_2=np.reshape(a[:,0],(-1,nlat_2))
t_2=np.reshape(a[:,1],(-1,nlat_2))
fld_2=np.reshape(a[:,2],(-1,nlat_2))

times=[150,200,250,350]  # Sync these to the times from Visit plots

#plt.axes([0.2,0.1,0.95-0.2,0.95-0.2])
for i in times:
    print t_1[i,0]
    p_2,=plt.plot(x_2[i,::2]-x_2[i,nlat_2/2-1],fld_2[i,::2],'r^',markeredgecolor='r',markerfacecolor='r')
    p_1,=plt.plot(x_1[i,::2]-x_1[i,nlat_1/2-1],fld_1[i,::2],'k')
 
plt.legend([p_1,p_2],[r"Fluctuations",r"No Fluctuations"],loc='lower center',bbox_to_anchor=(0.,0.8,1.,0.3),borderaxespad=0.)
plt.xlim(-90.,90.)
plt.ylim(-1.2,4.5)
plt.xlabel(r'$mx_\perp$')
plt.ylabel(r'$\phi/\phi_0$',labelpad=-0.8)

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
#plt.gca().set_position([0.15,0.15,0.95-0.15,0.95-0.15])

plt.savefig('plateau_compare_transverse.pdf')
if showPreview:
    plt.show()
plt.clf()

