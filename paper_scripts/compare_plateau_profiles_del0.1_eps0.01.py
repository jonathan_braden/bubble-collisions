#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from matplotlib.ticker import MaxNLocator

basedir='../data/plateau_del0.1_eps0.01/'
lattice_file_1=basedir+"collision_line_del0.1_eps0.01_wfluc.dat"
nlat_1=1024
lattice_file_2=basedir+"collision_line_del0.1_eps0.01_nofluc.dat"
nlat_2=1024

a=np.genfromtxt(lattice_file_1,usecols=[0,1,2])
fld_1=np.reshape(a[:,2],(-1,nlat_1))
x_1=np.reshape(a[:,0],(-1,nlat_1))
t_1=np.reshape(a[:,1],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[0,1,2])
x_2=np.reshape(a[:,0],(-1,nlat_2))
t_2=np.reshape(a[:,1],(-1,nlat_2))
fld_2=np.reshape(a[:,2],(-1,nlat_2))

#a=np.genfromtxt(field_1d,usecols=[0,1,2])
#x_1d=np.reshape(a[:,],(-1,nlat_1d))
#t_1d=np.reshape(a[:,],(-1,nlat_1d))
#fld_1d=np.reshape(a[:,],(-1,nlat_1d))

times=[150,200,250,350]  # Sync these to the times from Visit plots

#plt.axes([0.15,0.15,0.95-0.15,0.95-0.15])
for i in times:
    print t_2[i,0]
    p_2,=plt.plot(x_2[i,::2]-x_2[i,nlat_2/2-1],fld_2[i,::2],'r^',markeredgecolor='r',markerfacecolor='r')
    p_1,=plt.plot(x_1[i,:]-x_1[i,nlat_1/2-1],fld_1[i,:],'k')
 
plt.legend([p_1,p_2],[r"Fluctuations",r"No Fluctuations"],loc='lower center',bbox_to_anchor=(0.,0.8,1.,0.3),borderaxespad=0.)
plt.xlim(-90.,90.)
plt.ylim(-1.2,16.)
plt.gca().yaxis.set_major_locator(MaxNLocator(5))
plt.xlabel(r'$mx_\perp$')
plt.ylabel(r'$\phi/\phi_0$')

#plt.tight_layout(pad=0.25); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
#plt.gca().set_position([0.15,0.15,0.95-0.15,0.95-0.15])

plt.savefig('plateau_tilt_compare_transverse.pdf')
if showPreview:
    plt.show()
plt.clf()
