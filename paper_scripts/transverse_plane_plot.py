#!/usr/bin/env python
import matplotlib.pyplot as plt; import myplotutils as myplt
import matplotlib.cm as cm
import numpy as np
from matplotlib.ticker import MaxNLocator

plt_color = cm.RdYlBu_r

def gen_transverse_data(fname,nlat,cols):
    a=np.genfromtxt(fname,usecols=[cols[2]])
    fld=np.reshape(a,(-1,nlat))
    a=np.genfromtxt(fname,usecols=[cols[0]])
    xvals=np.reshape(a,(-1,nlat))
    a=np.genfromtxt(fname,usecols=[cols[1]])
    tvals=np.reshape(a,(-1,nlat))
    return xvals, tvals, fld

def transverse_plane_plot(xvals,tvals,fld,cvals,x_label=r'$mx_\perp$',y_label=r'$mt$',c_label=r'$\phi/\phi_0$',extend_u='both',cticks=[-1,0,1], xlims=(-65.,65.), ylims=(0.,80.)):
    c=plt.contourf(xvals,tvals,fld,cvals,cmap=plt_color,extend=extend_u)
    myplt.insert_rasterized_contour_plot(c)

    cb = plt.colorbar(orientation='vertical',ticks=cticks,extend=extend_u,pad=0.02)
    cb.ax.set_ylabel(c_label,labelpad=-0.8)
    cb.solids.set_rasterized(True)

    plt.xlim(xlims)
    plt.ylim(ylims)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.gca().yaxis.set_major_locator(MaxNLocator(5))

#    plt.tight_layout(pad=0.5); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())

if __name__=='__main__':
    print 'start'
