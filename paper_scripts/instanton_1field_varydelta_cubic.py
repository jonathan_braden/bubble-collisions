#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from scipy.optimize import fsolve

#delvals=[0.99,0.98,0.97,0.95,0.92,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.05]
delvals=[0.99,0.9,0.8,0.6,0.4,0.3,0.2,0.1,0.05]

infiles=[]
l=[]
basedir='../data/profiles_cubic/'
for d in delvals:
    infiles.append(basedir+"instanton_del"+str(d)+".dat")
    l.append(r"$\delta ="+str(d)+"$")

tau=[]
phi=[]
for f in infiles:
    a=np.genfromtxt(f,usecols=[1,2])
    tau.append(a[:,0])
    phi.append(a[:,1])

import matplotlib.colors as colors
import matplotlib.cm as cmx
from matplotlib.ticker import MaxNLocator

mycmap=plt.get_cmap('jet')
cNorm=colors.Normalize(vmin=0,vmax=len(delvals)-1)
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

plt.xlabel(r'$r_E$')
plt.ylabel(r'$\phi/\phi_0$',labelpad=-0.8)
for i in range(len(tau)):
    curcol=scalarMap.to_rgba(i)
    plt.plot(tau[i],phi[i],color=curcol,label=l[i])
plt.xlim(0.,80.)
plt.gca().xaxis.set_major_locator(MaxNLocator(6))

plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.tight_layout(pad=0.25); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('instanton_1field_cubic_varydelta.pdf')
if showPreview:
    plt.show()
plt.clf()

delout=np.linspace(0.,1.,51)
phi_true=np.ones(len(delout))
phi_out=[]
for i in range(len(delvals)):
    phi_out.append(phi[i][0])

plt.plot(delvals,phi_out,'ro',label=r'$\phi_{out}/\phi_0$')
plt.plot(delout,phi_true,label=r'$\phi_{true}/\phi_0$')
plt.xlabel(r'$\delta$')
plt.ylabel(r'$\phi/\phi_0$',labelpad=-0.8)
plt.xlim(0,1)
plt.ylim(-1.1,1.1)
plt.legend(loc='center left')
#plt.tight_layout(pad=0.25); myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.savefig('phitunnel_cubic_varydelta.pdf')
if showPreview:
    plt.show()
plt.clf()
