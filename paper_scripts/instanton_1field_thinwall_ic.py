#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from scipy.optimize import fsolve
from matplotlib.ticker import LogLocator, MaxNLocator

basedir='../data/linear_del0.1/'
infile = basedir+"instanton_w0.35.dat"
nlat=512

delta=0.1
rinit=2.**0.5/delta

potp_l = lambda x : x*(x**2-1.) - delta
potp_c = lambda x : (x+delta)*(x**2-1.)**2

phi_guess=1.+0.5*delta
phit=fsolve(potp_l,phi_guess)
phi_guess=-1.+0.5*delta
phif=fsolve(potp_l,phi_guess)
print "Vacua are ",phit, phif
phit=phit[0.]
phif=phif[0.]

def thinwall_linear(x):
    f = 0.5*(phif-phit)*np.tanh((x-rinit)/2.**0.5) + 0.5*(phif+phit)
    return f
def thinwall_cubic(x):
    f = -np.tanh((x-1.5*rinit)/2.**0.5)
    return f

def thinwall_badvac(x):
    f = -np.tanh((x-rinit)/2.**0.5) + 0.5*delta
    return f

def eom(x):
    f = 0.
    return f

tau=np.genfromtxt(infile,usecols=[1])
phi=np.genfromtxt(infile,usecols=[2])

thinwall=thinwall_linear(tau)
badvac = thinwall_badvac(tau)

plt.xlabel(r'$r_E$')
plt.ylabel(r'$\phi/\phi_0$',labelpad=-0.8)
plt.plot(tau,phi,label=r'Numerical')
plt.plot(tau,thinwall,label=r'Thin-Wall')
plt.plot(tau,badvac,'r.',label=r'$\phi_{f/t}=\mp\phi_0+\frac{\delta}{2}$')
plt.xlim(0.,4./delta)
plt.ylim(-1.3,1.3)
plt.gca().yaxis.set_ticks([-1,0,1])
plt.gca().xaxis.set_major_locator(MaxNLocator(4))
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('instanton_linear_del0.1.pdf')
if showPreview:
    plt.show()
plt.clf()

coeffs=np.genfromtxt(infile,usecols=[4])
plt.plot(np.abs(coeffs))
plt.xlabel(r'Mode Number $(i)$')
plt.ylabel(r'$|c_i|$')
plt.yscale('log')
plt.xlim((0,90))
plt.ylim(1.e-16,10.)
plt.gca().yaxis.set_ticks([1.e-15,1.e-10,1.e-5,1.])
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('instanton_linear_coeffs_del0.1.pdf')
if showPreview:
    plt.show()
plt.clf()

# Finally show volation of equation of motion
a=np.genfromtxt(basedir+'instanton_offgrid_linear_del0.1.dat',usecols=[0,2,3,4,5])
plt.plot(a[:,0],np.abs(a[:,4]),label='Numerical')
a=np.genfromtxt(basedir+'instanton_linear_del0.1_thinwall.dat',usecols=[0,2,3,4,5])
plt.plot(a[:,0],np.abs(a[:,4]),label='Thin-Wall')
a=np.genfromtxt(basedir+'instanton_linear_del0.1_badvac.dat',usecols=[0,2,3,4,5])
plt.plot(a[:,0],np.abs(a[:,4]),label=r'$\phi_{f/t}=\mp\phi_0+\frac{\delta}{2}$')

plt.yscale('log')
plt.ylabel(r"$EOM$",labelpad=0.25)
plt.xlabel(r'$r_E$')
plt.xlim(0,100)
plt.ylim(1.e-16,1.)
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
plt.legend(loc='center right')
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('instanton_linear_del0.1_eom.pdf')
if showPreview:
    plt.show()
plt.clf()
