#!/bin/bash

#module load python/2.7-latest
PREVIEW=False

sed 's/FIG_SIZE/figure.figsize : 1.90, 1.18/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.3' >> matplotlibrc
echo 'figure.subplot.left   : 0.29' >> matplotlibrc
echo 'figure.subplot.top    : 0.9' >> matplotlibrc
echo 'figure.subplot.right  : 0.9' >> matplotlibrc
./instanton_path.py  $PREVIEW

# The actual plotting commands
# 2.68, 1.8
sed 's/FIG_SIZE/figure.figsize : 2.86, 1.92/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.21' >> matplotlibrc
echo 'figure.subplot.left   : 0.21' >> matplotlibrc
echo 'figure.subplot.top    : 0.9' >> matplotlibrc
echo 'figure.subplot.right  : 0.96' >> matplotlibrc
./pot_plot_cubic.py $PREVIEW
./pot_plot_linear.py $PREVIEW
./one_bubble_profile.py $PREVIEW
./plot_transverse_plane_halfsize.py $PREVIEW
# Single bubble w/ thin-wall locations needs same vertical direction as one_bubble_profile.py

# shift these plots to the left
./instanton_1field_varydelta_linear.py $PREVIEW
./instanton_1field_varydelta_cubic.py $PREVIEW
./instanton_1field_thinwall_varydelta.py $PREVIEW # make the margins smaller to accomodate a 9pt legend ..., check what I have to sync it with, might require splitting the script
./instanton_1field_thinwall_ic.py $PREVIEW
./compare_plateau_profiles_del0.1_eps0.01.py $PREVIEW
./compare_plateau_profiles_del0.1.py $PREVIEW
#./instanton_path.py $PREVIEW

# shifted margins
sed 's/FIG_SIZE/figure.figsize : 2.86, 1.92/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.21' >> matplotlibrc
echo 'figure.subplot.left   : 0.15' >> matplotlibrc
echo 'figure.subplot.top    : 0.9' >> matplotlibrc
echo 'figure.subplot.right  : 0.9' >> matplotlibrc
./plot_field_at_origin_1bubble_thickwall.py $PREVIEW
./one_bubble_profile_cubic.py $PREVIEW

# Plots that are 0.6\linewidth
sed 's/FIG_SIZE/figure.figsize : 3.57, 2.21/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.2' >> matplotlibrc
echo 'figure.subplot.left   : 0.15' >> matplotlibrc
echo 'figure.subplot.top    : 0.94' >> matplotlibrc
echo 'figure.subplot.right  : 0.95' >> matplotlibrc
./plot_potential_tunnel.py $PREVIEW
./plot_transverse_planes.py $PREVIEW # fix up the one plot in here to be 0.48\linewidth
#./thick_wall_floquet.py $PREVIEW
./pot_plot_2field.py $PREVIEW

sed 's/FIG_SIZE/figure.figsize : 4.46, 2.81/g' matplotlibrc-base > matplotlibrc
echo 'figure.subplot.bottom : 0.2' >> matplotlibrc
echo 'figure.subplot.left   : 0.15' >> matplotlibrc
echo 'figure.subplot.top    : 0.94' >> matplotlibrc
echo 'figure.subplot.right  : 0.95' >> matplotlibrc
./plot_field_at_origin_wbadvac.py $PREVIEW

# If LaTeX is used to typeset the tick labels, use pdfcairo to reduce the figure size by removing extraneous embedded fonts
# This produces slightly larger file sizes than not typesetting with LaTeX but makes nicer figures

mkdir check_size
cp *.pdf check_size

#for f in *.pdf
#do
#    gs -dBATCH -dNOPAUSE -dSAFER -dQUIET -dNOPLATFONTS -dSubsetFonts=true -sDEVICE=pdfwrite -sOutputFile=tmp.pdf $f
#    mv tmp.pdf $f
#done

#for f in stuff
#pdfcairo -pdf -nocrop -origpagesizes -r 250 *.pdf .pdf

