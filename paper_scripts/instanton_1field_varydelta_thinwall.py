#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from scipy.optimize import fsolve

delvals=[0.3848,0.3845,0.384,0.38,0.37,0.35,0.33,0.25,0.2,0.15,0.1,0.05]
basedir='../data/profiles_linear/'

infiles=[]
l=[]
for d in delvals:
    infiles.append(basedir+"instanton_linear_del"+str(d)+".dat")
    l.append(r"$\delta ="+str(d)+"$")

tau=[]
phi=[]
for f in infiles:
    a=np.genfromtxt(f,usecols=[1,2])
    tau.append(a[:,0])
    phi.append(a[:,1])

import matplotlib.colors as colors
import matplotlib.cm as cmx

mycmap=plt.get_cmap('jet')
#cNorm=colors.Normalize(vmin=0.,vmax=delvals[0]) # normalize via delta
cNorm=colors.Normalize(vmin=0,vmax=len(delvals))#normalize via counting
scalarMap=cmx.ScalarMappable(norm=cNorm,cmap=mycmap)

plt.xlabel(r'$r_E$')
plt.ylabel(r'$\phi/\phi_0$')
for i in range(len(tau)):
    curcol=scalarMap.to_rgba(i)
    plt.plot(tau[i],phi[i],color=curcol,linewidth=1.5,label=l[i])
plt.xlim(0.,50.)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.1,1.1))
plt.tight_layout(); myplt.fix_axes_aspect( plt.gcf(),plt.gca() )
plt.savefig('instanton_1field_linear_varydelta.pdf')
if showPreview:
    plt.show()
plt.clf()

# Now extract the value at the origin (here approximated as innermost collocation point

phi_in=[]
for i in range(len(delvals)):
    phi_in.append(phi[i][0])

phi_true=[]
deltrue=np.linspace(0.,2./3.**1.5,51)
for d in deltrue:
    potp = lambda x : x*(x**2-1.) - d
    phi_true.append(fsolve(potp,[1])[0])

plt.plot(delvals,phi_in,'ro',markersize=5, label=r'$\phi_{out}/\phi_0$')
plt.plot(deltrue,phi_true,'b',linewidth=1.5, label=r'$\phi_{true}/\phi_0$')
plt.xlabel(r'$\delta$')
plt.ylabel(r'$\phi/\phi_0$')
plt.legend(loc='center left')
plt.xlim(0.,0.4)

plt.tight_layout(); myplt.fix_axes_aspect(plt.gcf(),plt.gca())

plt.savefig('phitunnel_linear_varydelta.pdf')
if showPreview:
    plt.show()
plt.clf()
