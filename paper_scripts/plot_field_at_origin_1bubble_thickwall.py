#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

basedir='../data/cubic_del0.99/'
lattice_file_1=basedir+"collision_line_n512_lattice.dat"
nlat_1=512
lattice_file_2=basedir+"collision_line_n1024.dat"
nlat_2=1024
file_1d=basedir+"collision_line_1d.dat"
nlat_1d=1024

a=np.genfromtxt(lattice_file_1,usecols=[1,2])
fld_1=np.reshape(a[:,1],(-1,nlat_1))
t_1=np.reshape(a[:,0],(-1,nlat_1))

a=np.genfromtxt(lattice_file_2,usecols=[1,2])
t_2=np.reshape(a[:,0],(-1,nlat_2))
fld_2=np.reshape(a[:,1],(-1,nlat_2))

a=np.genfromtxt(file_1d,usecols=[0,2])
fld_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

import matplotlib.pyplot as plt; import myplotutils as myplt

plt.plot(t_1[::8,nlat_1/2-1],fld_1[::8,nlat_1/2-1],'g^',alpha=0.75,label='3D Lattice, $N_{lat}=512^3$')
plt.plot(t_2[::4,nlat_2/2-1],fld_2[::4,nlat_2/2-1],'bv',alpha=0.75, label='3D Lattice, $N_{lat}=1024^3$')
plt.plot(t_1d[::,nlat_1d/2-1],fld_1d[::,nlat_1d/2-1],'r',label='SO(2,1) Lattice')
plt.legend(loc='lower right',bbox_to_anchor=(0,0,1.12,1))
plt.xlim(0,90.)
plt.ylim(-1.,2.)
plt.gca().set_yticks([-1,0,1,2])
plt.xlabel(r'$mt$')
plt.ylabel(r'$\phi/\phi_0$',labelpad=-0.8)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(),plt.gca() )

plt.savefig('one_bubble_del0.99_center.pdf')
if showPreview:
    plt.show()
plt.clf()
