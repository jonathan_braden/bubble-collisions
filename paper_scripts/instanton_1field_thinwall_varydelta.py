#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from scipy.optimize import fsolve
from matplotlib.ticker import LogLocator, MaxNLocator

#infile = "instanton_w0.35.dat"
basedir='../data/thin_walls/'
infiles = [ "instanton_linear_del0.005_w0.022_200modes.dat", "instanton_linear_del0.001_w0.0045_200modes.dat", "instanton_cubic_del0.01_w0.03_200modes.dat", "instanton_cubic_del0.005_w0.014_200modes.dat" ]
labels = [ r"$\delta = 0.005$, lin", r"$\delta=0.001$, lin", r"$\delta=0.01$,  cub", r"$\delta=0.005$, cub" ]
nlat=512

tau=[]
phi=[]
coeff=[]
for fcur in infiles:
    f = basedir+fcur
    tau.append(np.genfromtxt(f,usecols=[1]))
    phi.append(np.genfromtxt(f,usecols=[2]))
    coeff.append(np.genfromtxt(f,usecols=[4]))

for i in range(len(tau)):
    plt.plot(tau[i],phi[i],label=labels[i])
plt.xlabel(r'$r_E$')
plt.ylabel(r'$\phi/\phi_0$',labelpad=-0.8)
plt.legend(loc='center',bbox_to_anchor=(0.08,0,1,1))
plt.xlim(0,1600.)
plt.gca().xaxis.set_ticks([0,500,1000,1500])
plt.ylim(-1.2,1.2)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('thin_wall_bubbles.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in range(len(coeff)):
    plt.plot(np.abs(coeff[i]),label=labels[i])
plt.xlim(0,150)
plt.yscale('log')
plt.ylim(1.e-16,1.)
plt.ylabel(r'$|c_i|$',labelpad=-0.8)
plt.xlabel(r'Mode Number $(i)$')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.1)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('thin_wall_coeffs.pdf')
if showPreview:
    plt.show()
plt.clf()
