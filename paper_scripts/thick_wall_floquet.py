#!/usr/bin/python

from scipy.integrate import odeint
import numpy as np

period = 2.*np.pi
#period =   # for Lame equation

# Set the initial conditions
xinit = [ 1., 0., 0., 1., 1., 0. ]

t = [0, period ]

avals = np.linspace(0.,5.,501)
bvals = np.linspace(0.,1.,501)
#avals = np.linspace(0.,10., 101)
#bvals = np.linspace(0.5,0.5,1)
floquet=[]

# Open file for writing
fwr=open("mathieu_exp.dat",'w')

for a in avals:
    col = []
    for b in bvals:
        def func(y,t):
#            return [ y[1], (-a-4*y[5]-1.5*y[5]**2)*y[0], y[3], (-a-4*y[5]-1.5*y[5]**2)*y[2], y[5], -y[4] ]
            return [ y[1],(-a-4.*y[5])*y[0],y[3],(-a-4.*y[5])*y[2], y[5], -y[4]]
        xinit = [ 1., 0., 0., 1., b, 0. ]
        x = odeint(func, xinit, t)
      
        Q = np.abs(0.5*(x[1][0] + x[1][3]))
        mu = 0.
        if (Q**2 > 1.):
            mu = np.log(Q + np.sqrt(Q**2-1.))
        col.append(mu)

    print "done a "+str(a)
    floquet.append(col)

# Now I've got everything to construct the fundamental matrix
# Get the value of the exponent
import matplotlib.pyplot as plt; import myplotutils as myplt
import matplotlib.cm as cm

clevs=np.linspace(0,np.array(floquet).max(),25)
c=plt.contourf(bvals,avals,floquet, clevs,cmap=cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
#plt.pcolormesh(bvals,avals,floquet,cmap=cm.OrRd)
plt.xlim((0.,1.))
plt.ylim((0.,5.))
cb=plt.colorbar(orientation='vertical',pad=0.02)#,format='%.2f')
cb.solids.set_rasterized(True)
cb.set_ticks([0,1,2,3,4])
cb.set_ticklabels([0,1,2,3,4])
plt.contour(bvals,avals,floquet, [0],colors='k')
plt.xlabel(r'$B$')
plt.ylabel(r'$A$')
cb.ax.set_ylabel(r'$\mu T$')

#plt.tight_layout(pad=0.25)
plt.savefig('floquet_thick_wall.pdf')
plt.show()
