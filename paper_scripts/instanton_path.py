#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from scipy.optimize import fsolve
from matplotlib.ticker import MaxNLocator, LogLocator

basedir='../data/2field_cubic_del0.2_eps0.01/'
infile = basedir+"instanton_2field_eps-0.01_del0.2_cubic_w0.5.dat"
nlat=100
delta=0.2
epsilon=-0.01
g2=1.

levels=np.linspace(-0.1,2.5,26)
levs=[]
for i in range(len(levels)):
    levs.append(levels[i])
levs.append(3)
levs.append(4)
levs.append(5)

def potential_cubic(phi,chi,d,g,ep):
    f=0.25*(chi**2-1.)**2 + d*(chi**3/3.-chi+2./3.) + 0.5*g*phi**2*(chi-1)**2 + ep*phi
    return f

def potential(phi,chi,d, g, ep):
    f=0.25*(chi**2-1.)**2 - d*(chi-1.) + 0.5*g*phi**2*(chi-1)**2 + ep*phi
    return f

tau=np.genfromtxt(infile,usecols=[1])
chi=np.genfromtxt(infile,usecols=[2])
phi=np.genfromtxt(infile,usecols=[3])

phivals=np.linspace(-0.8,0.8,101)
chivals=np.linspace(-1.6,2,201)

potvals=[]
for i in range(len(phivals)):
    potvals.append(potential_cubic(phivals[i],chivals,delta,g2,epsilon))

c=plt.contourf(chivals,phivals,potvals,levels, cmap=plt.cm.OrRd_r, extend="max")
myplt.insert_rasterized_contour_plot(c)
#plt.pcolormesh(chivals,phivals,potvals,cmap=plt.cm.OrRd_r,rasterized=True)
cbar=plt.colorbar(orientation='vertical',extend='max',pad=0.02)
cbar.set_ticks([0,1,2])
cbar.ax.set_ylabel(r'$V/\lambda_\sigma\sigma_0^4$')
cbar.solids.set_rasterized(True)

plt.xlim(-1.6,1.6)
plt.ylim(-0.8,0.8)

plt.gca().xaxis.set_ticks([-1,0,1])
plt.gca().yaxis.set_major_locator(MaxNLocator(5))

plt.xlabel(r'$\sigma/\sigma_0$',labelpad=0.75)
plt.ylabel(r'$\phi/\sigma_0$',labelpad=-0.8)
plt.plot(chi,phi,color='b',linewidth=1)

#plt.tight_layout(pad=0.25); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('2field_path_cubic_del0.2_eps-0.01.pdf')
if showPreview:
    plt.show()
plt.clf()

plt.plot(tau,phi,label=r'$\phi/\sigma_0$')
plt.plot(tau,chi,label=r'$\sigma/\sigma_0$')
plt.xlabel(r'$r_E$',labelpad=0.75)
plt.ylabel(r'$\phi/\sigma_0,\sigma/\sigma_0$',labelpad=0.75)
plt.xlim(0,5./delta)
plt.ylim(-1.1,1.1)
leg=plt.legend(loc='upper right', bbox_to_anchor=(0,0,1,1), bbox_transform=plt.gcf().transFigure, borderaxespad=0.02)

#plt.tight_layout(pad=0.25); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('2field_fields_cubic_del0.2_eps-0.01.pdf')
if showPreview:
    plt.show()
plt.clf()

chispec=np.genfromtxt(infile,usecols=[6])
phispec=np.genfromtxt(infile,usecols=[7])

plt.plot(np.abs(phispec),label=r'$\phi/\sigma_0$')
plt.plot(np.abs(chispec),label=r'$\sigma/\sigma_0$')
plt.xlabel(r'Mode Number ($i$)',labelpad=0.75, fontsize=10)
plt.ylabel(r'$|c_i|$',labelpad=0.75)
plt.yscale('log')
leg=plt.legend(loc='upper right', bbox_to_anchor=(0,0,1,1), bbox_transform=plt.gcf().transFigure, borderaxespad=0.02)
plt.xlim(0.,100.)
plt.ylim(0.5e-19,2.)
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
#plt.tight_layout(pad=0.25); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('2field_spec_cubic_del0.2_eps-0.01.pdf')
if showPreview:
    plt.show()
plt.clf()
