#!/usr/bin/env python
import sys
print sys.argv
showPreview=(sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
from matplotlib.ticker import MaxNLocator

from transverse_plane_plot import *

def main():
    basedir = '../data/linear_del0.1/'
    infile=basedir+'one_bubble_exact_wfluc.dat'
    nlat=512
    xvals,tvals,fld = gen_transverse_data(infile,nlat,[0,1,2])
    xvals = center_positions(xvals)
    eps=0.1
    cvals = np.arange(-1.2,1.2+eps,eps)
    
    transverse_plane_plot(xvals,tvals,fld,cvals,xlims=(-45.,45.),ylims=(0.,45.))
# Add in the thin-wall locations
    rinit=2.**0.5*10.
    y_thinwall=np.linspace(0.,45.,101)
    x_thinwall=np.sqrt(rinit**2+y_thinwall**2)
    plt.plot(x_thinwall,y_thinwall,'k--',linewidth=1)
    plt.plot(-x_thinwall,y_thinwall,'k--',linewidth=1)
    plt.savefig('one_bubble_linear_del0.1_plane'+'.pdf')
    if showPreview:
        plt.show()
    plt.clf()
    return

def add_fixed_s(svals):
    chivals=np.linspace(-3.,3.)
    x=[]
    y=[]
    for i in range(len(svals)):
        x.append(svals[i]*np.sinh(chivals))
        y.append(svals[i]*np.cosh(chivals))
    for i in range(len(svals)):
        plt.plot(x[i],y[i],'k',alpha=1.,linewidth=0.5)
    return

def center_positions(xvals):
    for i in range(len(xvals)):
        xvals[i,:] = xvals[i,:] - xvals[i,len(xvals[i])/2-1]
    return xvals

if __name__=='__main__':
    main()
