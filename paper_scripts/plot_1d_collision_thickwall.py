#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

from transverse_plane_plot import *

basedir='../data/cubic_del0.99/'
infile=basedir+"two_bubble_1dsim.dat"
nlat=1024

xvals,tvals,fld = gen_transverse_data(infile,nlat,[1,0,2])
# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
tvals=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[1])
xvals=np.reshape(a,(-1,nlat))

import matplotlib.cm as cm
from transverse_plane_plot import *

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)

transverse_plane_plot(xvals,tvals,fld,cvals,x_label=r'$mx_\parallel$')
#c=plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r)
#myplt.insert_rasterized_contour_plot(c)
   #plt.pcolormesh(xvals,tvals,fld,cmap=cm.RdYlBu_r,vmin=-1.2,vmax=1.2,rasterized=True)

#cb=plt.colorbar(orientation='vertical',ticks=[-1,0,1],extend='both',pad=0.02)
#cb.ax.set_ylabel(r'$\phi/\phi_0$') #,rotation='horizontal')
#cb.solids.set_rasterized(True)

plt.xlim((-65.,65.))
plt.ylim((0.,80.))

plt.xlabel(r'$mx_{\perp}$')
plt.ylabel(r'$mt$')

plt.tight_layout(pad=0.1); #myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig("two_bubble_1d_del0.99_collision.pdf")
if showPreview:
    plt.show()
plt.clf()
