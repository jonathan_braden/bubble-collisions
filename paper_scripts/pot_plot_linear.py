#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np
#
# Define the potential parameters in here
#
delvals=(0., 1./30., 1./10., 1./5., 2./3.**1.5)
print len(delvals)
dellabels=(r'$0$',r'$\frac{1}{30}$',r'$\frac{1}{10}$',r'$\frac{1}{5}$',r'$\frac{2}{3^{3/2}}$')

# Define the potential
def pot(x, d):
    return 0.25*(x**2-1)**2 - d*(x-1.)

#
# Now make the plot
#
plt.ylabel(r'$V(\phi)/\lambda \phi_0^4$')
plt.xlabel(r'$\phi/\phi_0$')

xvals = np.linspace(-1.5,1.5,101)
for i in range(len(delvals)):
    yvals=pot(xvals, delvals[i])
    plt.plot(xvals, yvals, label=r'$\delta=$'+dellabels[i])
plt.xlim(-1.5,1.5)
plt.ylim(-0.2,1.7)
plt.gca().xaxis.set_ticks([-1,0,1])
plt.gca().yaxis.set_ticks([0,0.5,1,1.5])
#plt.legend(frameon=False)
plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.25)
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig('potential_linear.pdf')
if showPreview:
    plt.show()
plt.clf()
