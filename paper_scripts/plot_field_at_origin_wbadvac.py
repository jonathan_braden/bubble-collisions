#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

basedir='../data/linear_del0.1/'
lattice_file=basedir+"collision_line_exactic.dat"
nlat_lat=1024
file_1d=basedir+"field_exactic_1d.dat"
nlat_1d=1024
lattice_badvac=basedir+"collision_line_del0.1_badvac.dat"
lat_bv_noniso=basedir+"collision_line_del0.1_badvac_badstencil.dat"
file_1d_badvac=basedir+"field_badvac_1d.dat"

a=np.genfromtxt(lattice_file,usecols=[1,2])
fld_l=np.reshape(a[:,1],(-1,nlat_lat))
t_l=np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(file_1d,usecols=[0,2])
fld_1d=np.reshape(a[:,1],(-1,nlat_1d))
t_1d=np.reshape(a[:,0],(-1,nlat_1d))

a=np.genfromtxt(lattice_badvac,usecols=[1,2])
fld_l_bv = np.reshape(a[:,1],(-1,nlat_lat))
t_l_bv = np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(lat_bv_noniso,usecols=[1,2])
fld_l_bv_bs=np.reshape(a[:,1],(-1,nlat_lat))
t_l_bv_bs=np.reshape(a[:,0],(-1,nlat_lat))

a=np.genfromtxt(file_1d_badvac,usecols=[0,2])
fld_1d_bv=np.reshape(a[:,1],(-1,nlat_1d))
t_1d_bv=np.reshape(a[:,0],(-1,nlat_1d))

plt.plot(t_1d[:,nlat_1d/2-1],fld_1d[:,nlat_1d/2-1],'b',label='SO(2,1), Exact IC')
plt.plot(t_l[:,nlat_lat/2-1],fld_l[:,nlat_lat/2-1],'g^',label='3D, Exact IC')
plt.plot(t_1d_bv[:,nlat_1d/2-1],fld_l[:,nlat_lat/2-1],'r.',label='SO(2,1), Approx. IC')
plt.plot(t_l_bv[:,nlat_lat/2-1],fld_l_bv[:,nlat_lat/2-1],'k',label='3D, Approx. IC, Isotropic Stencil')
plt.plot(t_l_bv_bs[:,nlat_lat/2-1],fld_l_bv_bs[:,nlat_lat/2-1],color='orange',marker='v',label='3D, Approx. IC, Noniso. Stencil')
plt.legend(loc='lower left',borderaxespad=0.25)
plt.xlim(0,100.)
plt.xlabel(r'$mt$')
plt.ylabel(r'$\phi/\phi_0$')
plt.ylim(-4.9,3)
plt.gca().yaxis.set_ticks([-4,-2,0,2])
#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(),plt.gca() )
plt.savefig('field_origin_linear_del0.1_wbadvac.pdf')
if showPreview:
    plt.show()
plt.clf()
