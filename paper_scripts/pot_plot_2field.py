#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np

d=0.2
e=-0.01
g=1.

def potential(x,y,delta,eps,g2):
    f = 0.25*(x**2-1)**2 + delta*(x**3/3.-x+2./3.) + 0.5*g2*(x-1.)**2*y**2 - eps*y
    return f

phi=np.linspace(-0.8,0.8,151)
chi=np.linspace(-1.6,1.5,101)
pot=[]

for p in phi:
    pot.append(potential(chi,p,d,e,g))

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D

pot=np.array(pot)
pot[pot>1]=1.

fig=plt.figure()
ax=fig.add_subplot(111,projection='3d')
x,y=np.meshgrid(chi,phi)
ax.plot_surface(x,y,pot,cmap=cm.copper_r,vmin=0.,vmax=1.,linewidth=0.25,rasterized=False)
#ax.plot_wireframe(x,y,pot,rstride=8,cstride=8,linewidth=0.25,color='k')
ax.set_zlim3d(0.,1.)
ax.set_xlabel(r'$\sigma/\sigma_0$')
ax.set_ylabel(r'$\phi/\sigma_0$')
ax.set_zlabel(r'$V(\sigma,\phi)/\lambda_\sigma\sigma_0^4$')

ax.set_xlim(-1.6,1.5)
ax.set_ylim(-0.85,0.85)
ax.set_xticks([-1,0,1])
ax.set_yticks([-0.5,0,0.5])
ax.set_zticks([0,0.5,1])

ax.xaxis._axinfo['label']['space_factor'] = 2.5
ax.xaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.yaxis._axinfo['label']['space_factor'] = 2.5
ax.yaxis._axinfo['ticklabel']['space_factor'] = 0.5
ax.zaxis._axinfo['label']['space_factor'] = 2.5
ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.
plt.subplots_adjust(left=0.01,right=0.9,bottom=0.1,top=0.95)

plt.savefig('potential_2field.pdf')
if showPreview:
    plt.show()
plt.clf()
