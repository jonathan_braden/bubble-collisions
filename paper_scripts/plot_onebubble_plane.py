#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import matplotlib.pyplot as plt; import myplotutils as myplt
import numpy as np

basedir='../data/linear_del0.1/'
infile=basedir+"one_bubble_exact_wfluc.dat"
nlat=512

# Get data from file
a=np.genfromtxt(infile,usecols=[2])
fld=np.reshape(a,(-1,nlat))
a=np.genfromtxt(infile,usecols=[0])
xvals=a[0:nlat]
a=np.genfromtxt(infile,usecols=[1])
tvals=a[0::nlat]

if (xvals[0] > 0.):
    xvals=xvals-xvals[nlat/2-1] # center collision at x=0

rinit=2.**0.5*10.
y_thinwall=np.linspace(0.,45.,101)
x_thinwall=np.sqrt(rinit**2+y_thinwall**2)

import matplotlib.cm as cm

eps=0.1
cvals=np.arange(-1.2,1.2+eps,eps)
c=plt.contourf(xvals,tvals,fld,cvals,cmap=cm.RdYlBu_r,extend='both')
myplt.insert_rasterized_contour_plot(c)
#plt.pcolormesh(xvals,tvals,fld,cmap=cm.RdYlBu_r,vmin=-1.2,vmax=1.2,rasterized=True)

cb=plt.colorbar(orientation='vertical',extend='both',ticks=[-1,0,1])
cb.ax.set_ylabel(r'$\phi/\phi_0$')#,rotation='horizontal')
cb.solids.set_rasterized(True)

plt.plot(x_thinwall,y_thinwall,'k--',linewidth=1.)
plt.plot(-x_thinwall,y_thinwall,'k--',linewidth=1.)

plt.xlim((-45.,45.))
plt.ylim((0.,45.))

plt.xlabel(r'$mx_{\perp}$')
plt.ylabel(r'$mt$')

#plt.subplots_adjust(bottom=0.15,left=0.15)
plt.tight_layout(pad=0.05); myplt.fix_axes_aspect(plt.gcf(),plt.gca())
plt.savefig("one_bubble_linear_del0.1_plane.pdf")

if showPreview:
    plt.show()
plt.clf()
